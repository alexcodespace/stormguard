<?php
/**
 * The template for displaying archive services pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-services" id="main">
		<div class="bg-page" <?php if(get_field('head_background', 'option')):?>
			style="background: url('<?php the_field('head_background', 'option');?>');
			background-attachment: fixed;
			background-position: top;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<div class="container" id="content" tabindex="-1">
				<div class="row">
					<div class="col-xl-12">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						}
						?>
					</div>
					<?php if ( have_posts() ) : ?>
						<div class="col-12 text-center">
							<h2><?php post_type_archive_title();?></h2>
						</div>
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="col-xl-4 text-center">
								<?php if(get_field('short_description')): ?>
								<div class="services-icon">
									<img src="<?php the_field('icon');?>" alt="">
								</div>
								<?php endif;?>

								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

								<?php if(get_field('short_description')): ?>
									<p><?php the_field('short_description');?></p>
								<?php endif;?>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div> <!-- .row -->
			</div><!-- .container -->
		</div>
	</main><!-- #main -->


<?php get_footer();
