<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-project-single" id="main">
		<div class="bg-page" <?php if(get_field('blog_background', 'option')):?>
			style="background: url('<?php the_field('blog_background', 'option');?>');
			background-attachment: scroll;
			background-position: center;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>

		<div class="sg-wrapper" >
			<section class="sg-content">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
							?>
						</div>
						<div class="col-12">
							<h2><?php the_title();?></h2>
							<p><?php the_content();?></p>
						</div>

					</div>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php $images = get_field('gallery'); /*<-- Enter Gallery name here-->*/
					if( $images ): ?>
						<div class="row" id="lightgallery">
							<?php foreach( $images as $image ): ?>
								<a class="col-xl-3 text-center" href="<?php echo $image['url']; ?>">
									<img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<?php endwhile; // end of the loop. ?>
				</div>
			</section>
		</div>
		<?php get_template_part( 'loop-templates/content', 'started' );?>
		<?php get_template_part( 'loop-templates/content', 'find' );?>
	</main>

<?php get_footer();
