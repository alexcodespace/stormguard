<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-service-single" id="main">
		<div class="bg-page" <?php if(get_field('header_background')):?>
			style="background: url('<?php the_field('header_background');?>');
			background-attachment: scroll;
			background-position: center;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<section class="sg-content">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
					<?php while ( have_posts() ) : the_post(); ?>
					<article class="sg-content-single">
						<div class="row">
							<div class="col-xl-5">
								<?php the_post_thumbnail('full');?>
							</div>
							<div class="col-xl-7">
								<h2><?php the_title();?></h2>

								<?php the_content();?>
							</div>
						</div>
					</article>
					<?php endwhile; // end of the loop. ?>
				</div>
			</section>
			<section class="sg-services">
				<div class="container">
					<div class="row text-center">
						<div class="col-md-12">
							<?php if(get_field('title_services', 'option')): ?>
								<h2><?php the_field('title_services', 'option');?></h2>
							<?php endif;?>
							<?php if(get_field('description_services', 'option')): ?>
								<p><?php the_field('description_services', 'option');?></p>
							<?php endif;?>
						</div>
						<?php $arg = array(
							'post_type'	    => 'services', /*<-- Enter name of Custom Post Type here*/
							'order'		    => 'ASC',
							'orderby'	    => 'menu_order',
							'posts_per_page'    => -1,
							'post__not_in' => array($post->ID)
						);

						$the_query = new WP_Query( $arg );
						if ( $the_query->have_posts() ) : ?>

								<?php while ( $the_query->have_posts() ) : $the_query->the_post();
								$do_not_duplicate = $post->ID; ?><!-- BEGIN of Post -->
								<div class="col-xl-4">
									<?php if(get_field('short_description')): ?>
										<div class="services-icon">
											<img src="<?php the_field('icon');?>" alt="">
										</div>
									<?php endif;?>

									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

									<?php if(get_field('short_description')): ?>
										<p><?php the_field('short_description');?></p>
									<?php endif;?>
								</div>
							<?php endwhile;?>
						<?php endif; wp_reset_query(); ?>
					</div><!-- END of .post-type -->
				</div>
			</section>
		</div>
		<?php get_template_part( 'loop-templates/content', 'started' );?>
		<?php get_template_part( 'loop-templates/content', 'find' );?>
	</main>




<?php get_footer();
