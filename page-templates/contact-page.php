<?php
/**
 * Template Name: Contact Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-contact">
		<div class="bg-page" <?php if(get_field('head_background')):?>
			style="background: url('<?php the_field('head_background');?>');
			background-attachment: fixed;
			background-position: top;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						}
						?>
					</div>
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?><!-- BEGIN of Post -->
							<div class="col-md-6">
								<h2><?php the_title(); ?></h2>
								<?php if(get_field('shortcode_contact')):?>
									<?php echo do_shortcode(get_field('shortcode_contact'));?>
								<?php endif;?>
							</div>
							<div class="col-md-6">
								<?php the_post_thumbnail(); ?>
								<p><?php the_content(); ?></p>
							</div>
						<?php endwhile; ?><!-- END of Post -->
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php get_template_part( 'loop-templates/content', 'find' );?>
	</main>
<?php get_footer();
