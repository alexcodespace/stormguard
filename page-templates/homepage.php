<?php
/**
 * Template Name: Home Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="home-section">
	<section class="sg-head"
		<?php $image = get_field('background_head');
		if( !empty( $image ) ): ?>
			 style="background: url('<?php echo esc_url($image['url']); ?>');
				 	background-size: cover;
				 	background-attachment: scroll;
				 	background-position: center;"
		<?php endif; ?>>

		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-12">
					<div class="sg-caption">
						<?php if(get_field('description_head')):?>
							<?php the_field('description_head');?>
						<?php endif; ?>
						<?php if(get_field('link_to_contact')):?>
						<a class="contact-link" href="<?php the_field('link_to_contact'); ?>">Contact Us Today</a>
						<?php endif;?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php get_template_part( 'loop-templates/content', 'find' );?>

	<section class="sg-services">
		<div class="container">
			<div class="row justify-content-center" >
				<div class="col-lg-5 col-md-7 col-12">
					<?php if(get_field('title_offer')):?>
						<h2 class="text-center"><?php the_field('title_offer');?></h2>
					<?php endif; ?>
					<?php if(get_field('description_offer')):?>
						<p class="text-center"><?php the_field('description_offer');?></p>
					<?php endif; ?>
				</div>
			</div>

			<?php $arg = array(
				'post_type'	    => 'services', /*<-- Enter name of Custom Post Type here*/
				'order'		    => 'ASC',
				'orderby'	    => 'menu_order',
				'posts_per_page'    => -1
			);

			$the_query = new WP_Query( $arg );
			if ( $the_query->have_posts() ) : ?>
				<div class="row text-center">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post();
					$do_not_duplicate = $post->ID; ?><!-- BEGIN of Post -->
					<div class="col-xl-4 col-md-6 col-12">
						<?php if(get_field('short_description')): ?>
							<div class="services-icon">
								<img src="<?php the_field('icon');?>" alt="">
							</div>
						<?php endif;?>

						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

						<?php if(get_field('short_description')): ?>
							<p><?php the_field('short_description');?></p>
						<?php endif;?>
					</div>
					<?php endwhile;?>
				</div><!-- END of .row -->
			<?php endif; wp_reset_query(); ?>
			<div class="col-12 text-center">
				<a class="button-link" href="<?php echo get_post_type_archive_link('services');?>">View All Services</a>
			</div>
		</div>
	</section>

	<?php get_template_part( 'loop-templates/content', 'started' );?>

	<section class="sg-testimonials">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
				<?php if( get_field('title_testimonials')):?>
					<h2 class="text-center"><?php the_field('title_testimonials');?></h2>
				<?php endif; ?>
				</div>
			</div>
		</div>
	\
		<?php $arg = array(
			'post_type'	        => 'Testimonial',
			'order'		        => 'ASC',
			'orderby'	        => 'menu_order',
			'posts_per_page'    => -1
		);
		$testimonial = new WP_Query( $arg );
		if ( $testimonial->have_posts() ) : ?>
			<div id="testimonial-slide" class="slick-slider" >
				<?php while ( $testimonial->have_posts() ) : $testimonial->the_post(); ?>
					<div class="slick-slide">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="slider-caption">
										<?php if( get_field('description')):?>
											<p class="sg-testimonial-content"><?php the_field('description'); ?></p>
										<?php endif;?>
										<div class="sg-author">
											<?php if( get_field('name')):?>
												<p class="sg-testimonial-name"><?php the_field('name'); ?></p>
											<?php endif;?>
											<?php if( get_field('position')):?>
												<p><?php the_field('position'); ?></p>
											<?php endif;?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div><!-- END of  #home-slider-->
		<?php endif; wp_reset_query(); ?>
	</section>
	<section class="sg-blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php if( get_field('title_news')):?>
						<h2 class="text-center"><?php the_field('title_news');?></h2>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php $arg = array(
			'post_type'	    => 'post', /*<-- Enter name of Custom Post Type here*/
			'order'		    => 'ASC',
			'orderby'	    => 'menu_order',
			'posts_per_page'    => 3
		);

		$the_query = new WP_Query( $arg );
		if ( $the_query->have_posts() ) : ?>
		<div class="container">
			<div class="row text-center">
				<?php while ( $the_query->have_posts() ) : $the_query->the_post();
				$do_not_duplicate = $post->ID; ?><!-- BEGIN of Post -->
				<div class="col-md-4 col-12">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="sg-blog-image">
							<div class="post-time">
								<p class="post-month"><?php the_time('M');?></p>
								<p class="post-number"><?php the_time('j');?></p>
							</div>
							<?php the_post_thumbnail('blog-item');?>
						</div>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<p><?php the_excerpt(); ?></p>
						<p class="blog-author"><strong>BY AUTHOR</strong> <?php the_author();?></p>
					</article>
				</div>
				<?php endwhile;?>
				<div class="col-12">
					<a class="button-link" href="<?php echo get_post_type_archive_link('post');?>">View More News</a>
				</div>
			</div><!-- END of .row -->
		</div>
		<?php endif; wp_reset_query(); ?>

	</section>
</div>




<?php get_footer(); ?>
