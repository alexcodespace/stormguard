<?php
/**
 * Template Name: Franchise Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-franchise">
		<div class="bg-page" <?php if(get_field('background')):?>
			style="background: url('<?php the_field('background');?>');
			background-attachment: scroll;
			background-position: bottom;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						}
						?>
					</div>
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?><!-- BEGIN of Post -->
							<div class="col-xl-12">
								<?php the_content(); ?>
							</div>
						<?php endwhile; ?><!-- END of Post -->
					<?php endif; ?>
				</div>
			</div>
		</div>


	</main>
<?php get_footer();
