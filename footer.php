<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper-footer">
	<section class="sg-pre-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-12">
					<?php if(get_field('title_contact', 'option')):?>
						<h2><?php the_field('title_contact', 'option');?></h2>
					<?php endif; ?>
					<?php if(get_field('description_contact', 'option')):?>
						<p><?php the_field('description_contact', 'option');?></p>
					<?php endif; ?>
					<?php if(get_field('shortcode_contact_form', 'option')):?>
						<?php echo do_shortcode(get_field('shortcode_contact_form', 'option'));?>
					<?php endif;?>
				</div>
				<div class="col-md-6 col-12" >
					<?php if(get_field('title_projects', 'option')):?>
						<h2><?php the_field('title_projects', 'option');?></h2>
					<?php endif; ?>
					<?php if(get_field('description_projects', 'option')):?>
						<p><?php the_field('description_projects', 'option');?>
							<?php if(!is_front_page()):?>
							<a class="text-link" href="<?php echo get_post_type_archive_link('projects');?>">View the whole portfolio</a>
							<?php endif;?>
						</p>
					<?php endif; ?>
					<div class="row">
					<?php $arg = array(
						'post_type'	    => 'projects', /*<-- Enter name of Custom Post Type here*/
						'order'		    => 'ASC',
						'orderby'	    => 'menu_order',
						'posts_per_page'    => -1
					);

					$the_query = new WP_Query( $arg );
					if ( $the_query->have_posts() ) : ?>
						<?php while ( $the_query->have_posts() ) : $the_query->the_post();
						$do_not_duplicate = $post->ID; ?><!-- BEGIN of Post -->
						<div class="col-xl-3 col-md-6 col-sm-6 col-6">
							<a class="projects-link" href="<?php the_permalink();?>"><?php the_post_thumbnail($size = 'thumbnail');?></a>
						</div>
						<?php endwhile;?>
					<?php if(is_front_page()):?>
						<div class="col-12 text-center">
							<a class="button-link" href="<?php echo get_post_type_archive_link('projects');?>">View More Photos</a>
						</div>
					<?php endif;?>
					<?php endif; wp_reset_query(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="sg-footer">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-xl-6 col-12">
					<?php //understrap_site_info(); ?>
					<?php if(get_field('copyright', 'option') && get_field('copyright_link', 'option') && get_field('title_copyright_link', 'option')):?>
						<p class="copyright-text"><?php the_field('copyright', 'option')?> <a class="copyright-link" href="<?php the_field('copyright_link', 'option')?>"><?php the_field('title_copyright_link', 'option')?></a></p>
					<?php endif; ?>
				</div>
				<div class="col-xl-6 col-12">
					<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
						<nav class="navbar navbar-expand-md navbar-dark footer-menu">
					<?php wp_nav_menu(
						array(
							'theme_location'  => 'footer-menu',
							'container_class' => 'uncollapse footer-nav',
							'container_id'    => 'navbarNavDropdown',
							'menu_class'      => 'navbar-nav ml-auto',
							'fallback_cb'     => '',
							'menu_id'         => 'footer-menu',
							'depth'           => 3,
							'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
						)
					); ?>
						</nav><!-- .site-navigation -->
				</div>
			</div>
		</div>
	</footer>
</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

