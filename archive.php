<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>
	<main class="sg-blog" id="main">
		<div class="bg-page" <?php if(get_field('blog_background', 'option')):?>
			style="background: url('<?php the_field('blog_background', 'option');?>');
			background-attachment: fixed;
			background-position: top;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<div class="container" id="content" tabindex="-1">
				<div class="row text-center">
					<div class="col-xl-12">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
						}
						?>
					</div>
					<?php if ( have_posts() ) : ?>
						<?php if(get_field('title_blog', 'option')):?>
							<div class="col-12">
								<?php
								the_archive_title( '<h1 class="page-title">', '</h1>' );
								the_archive_description( '<div class="taxonomy-description">', '</div>' );
								?>
							</div>
						<?php endif; ?>
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="col-xl-4">
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="sg-blog-image">
										<div class="post-time">
											<p class="post-month"><?php the_time('M');?></p>
											<p class="post-number"><?php the_time('j');?></p>
										</div>
										<?php the_post_thumbnail('full');?>
									</div>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
									<p class="blog-author"><strong>BY AUTHOR</strong> <?php the_author();?></p>
								</article>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div> <!-- .row -->
			</div><!-- .container -->
		</div>
	</main><!-- #main -->

<?php get_footer();
