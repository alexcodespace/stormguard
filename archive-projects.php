<?php
/**
 * The template for displaying archive services pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-projects" id="main">
		<div class="bg-page" <?php if(get_field('head_background', 'option')):?>
			style="background: url('<?php the_field('head_background', 'option');?>');
			background-attachment: fixed;
			background-position: top;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<section class="sg-content">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
							?>
						</div>
						<?php if ( have_posts() ) : ?>
							<div class="col-12 text-center">
								<h2><?php post_type_archive_title();?></h2>
							</div>
							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<div class="col-xl-4">
									<article id="post-<?php the_ID(); ?>" class="sg-projects-link">
										<?php the_post_thumbnail('medium');?>
										<div class="link-img">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div>
									</article>
								</div>
							<?php endwhile; ?>
						<?php endif; ?>
					</div> <!-- .row -->
				</div><!-- .container -->
			</section>
		</div>
	</main><!-- #main -->


<?php get_footer();
