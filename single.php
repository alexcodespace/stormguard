<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-service-single" id="main">
		<div class="bg-page" <?php if(get_field('blog_background', 'option')):?>
			style="background: url('<?php the_field('blog_background', 'option');?>');
			background-attachment: scroll;
			background-position: center;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<section class="sg-content">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
							}
							?>
						</div>
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="col-xl-5">
								<?php the_post_thumbnail('full');?>
							</div>
							<div class="col-xl-7">
								<h2><?php the_title();?></h2>
								<?php the_content();?>
							</div>

						<?php endwhile; // end of the loop. ?>

					</div>
				</div>
			</section>
		</div>
		<?php get_template_part( 'loop-templates/content', 'started' );?>
		<?php get_template_part( 'loop-templates/content', 'find' );?>
	</main>

<?php get_footer();
