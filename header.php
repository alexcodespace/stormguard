<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action('wp_body_open'); ?>
<div class="site" id="page">
	<header class="sg-header">
		<!-- ******************* The Navbar Area ******************* -->
		<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

			<nav class="navbar navbar-expand-lg navbar-dark primary-menu">

				<div class="container">

					<?php if (get_field('title_link', 'option') && get_field('location_link', 'option')): ?>
						<div class="nav-item">
							<a class="nav-link loc-link"
							   href="<?php the_field('location_link', 'option') ?>"><?php the_field('title_link', 'option') ?>
								<i class="fa fa-angle-right" aria-hidden="true"></i></a>
						</div>
					<?php endif; ?>

					<!-- The WordPress Menu goes here -->
					<?php wp_nav_menu(
						array(
							'theme_location' => 'primary',
							'container_class' => 'collapse navbar-collapse',
							'container_id' => '',
							'menu_class' => 'navbar-nav ml-auto',
							'fallback_cb' => '',
							'menu_id' => 'main-menu',
							'depth' => 2,
							'walker' => new Understrap_WP_Bootstrap_Navwalker(),
						)
					); ?>

				</div><!-- .container -->


			</nav><!-- .site-navigation -->

		</div><!-- #wrapper-navbar end -->

		<!-- ******************* The Navbar SECOND Area ******************* -->
		<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
			<nav class="navbar navbar-expand-lg navbar-dark second-menu">
				<div class="container">
					<!-- Your site title as branding in the menu -->
					<?php if (!get_field('logo', 'option')) { ?>
						<?php if (is_front_page() && is_home()) : ?>
							<h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url(home_url('/')); ?>"
															 title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
															 itemprop="url"><?php bloginfo('name'); ?></a></h1>
						<?php else : ?>
							<a class="navbar-brand" rel="home" href="<?php echo esc_url(home_url('/')); ?>"
							   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"
							   itemprop="url"><?php bloginfo('name'); ?></a>
						<?php endif; ?>
					<?php } else {
					//the_custom_logo(); ?>
					<a class="navbar-brand" rel="home" href="<?php echo esc_url(home_url('/')); ?>"
					   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" itemprop="url">
						<img src="<?php the_field('logo', 'option') ?>"
							 alt="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="img-fluid">
					</a>
					<?php
					} ?><!-- end custom logo -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
							aria-controls="navbarNavDropdown" aria-expanded="false"
							aria-label="<?php esc_attr_e('Toggle navigation', 'understrap'); ?>">
						<span class="navbar-toggler-icon"></span>
					</button>
					<!-- The WordPress Menu goes here -->
					<?php wp_nav_menu(
						array(
							'theme_location' => 'second-menu',
							'container_class' => 'collapse navbar-collapse',
							'container_id' => '',
							'menu_class' => 'navbar-nav ml-auto',
							'fallback_cb' => '',
							'menu_id' => 'second-menu',
							'depth' => 2,
							'walker' => new Understrap_WP_Bootstrap_Navwalker(),
						)
					); ?>

					<?php wp_nav_menu(
						array(
							'theme_location' => 'mobile-menu',
							'container_class' => 'collapse navbar-collapse sg-mobile-menu',
							'container_id' => 'navbarNavDropdown',
							'menu_class' => 'navbar-nav ml-auto',
							'fallback_cb' => '',
							'menu_id' => 'main-menu',
							'depth' => 2,
							'walker' => new Understrap_WP_Bootstrap_Navwalker(),
						)
					); ?>


				</div><!-- .container -->


			</nav><!-- .site-navigation -->

		</div><!-- #wrapper-navbar end -->
	</header>
