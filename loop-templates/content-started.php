<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<section class="sg-started">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2><?php the_field('title', 'option');?></h2>
				<p><?php the_field('description', 'option');?></p>
			</div>
			<?php if( have_rows('links', 'option') ): ?>
				<?php while( have_rows('links', 'option') ): the_row();
					// Declare variables below
					$image = get_sub_field('image');
					$title = get_sub_field('title');
					$link = get_sub_field('link');
					// Use variables below ?>
					<div class="col-md-4 col-12">
						<div class="sg-item-link">
							<img src="<?php echo $image['sizes']['started-block']; ?>" />
							<div class="link-img">
								<a class="more" href="<?php echo $link; ?>"><?php echo $title; ?><i class="fa fa-angle-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div><!--end of .columns -->
				<?php endwhile; ?>
			<?php endif; ?>

		</div>
	</div>
</section>
