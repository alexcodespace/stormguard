<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

	<main class="sg-error-404">
		<div class="bg-page" <?php if(get_field('head_background', 'option')):?>
			style="background: url('<?php the_field('head_background', 'option');?>');
			background-attachment: fixed;
			background-position: top;
			background-size: cover;
			min-height: 30vh;
			"<?php endif;?>>
		</div>
		<div class="sg-wrapper" >
			<div class="container">
				<div class="row">
					<section class="error-404 not-found">

						<header class="page-header">

							<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'understrap' ); ?></h1>

						</header><!-- .page-header -->

						<div class="page-content">

							<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'understrap' ); ?></p>

						</div><!-- .page-content -->

					</section><!-- .error-404 -->
				</div>
		</div>
		<?php get_template_part( 'loop-templates/content', 'started' );?>
		<?php get_template_part( 'loop-templates/content', 'find' );?>
	</main>

<?php get_footer();
